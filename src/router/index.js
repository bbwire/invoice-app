import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router)

export default new Router({ 
  routes: [
    { path: '/', name: 'home', component:  () => import('@/views/Home.vue'), meta: { title: 'Home' } },
    { path: '/customers/', name: 'customers', component:  () => import('@/views/Customers.vue'), meta: { title: 'Customers' } },
    { path: '/customer/invoices/:id', name: 'customer-invoices', component:  () => import('@/views/CustomerInvoices.vue'), meta: { title: 'Customer Invoices' } },
    { path: '/customer/:cid/invoice/:id', name: 'invoice-details', component:  () => import('@/views/InvoiceDetails.vue'), meta: { title: 'Invoice Details' } },
    { path: '/invoices/', name: 'invoices', component:  () => import('@/views/Invoices.vue'), meta: { title: 'Invoices' } },
    { path: '/products/', name: 'products', component:  () => import('@/views/Products.vue'), meta: { title: 'Products' } },
    { path: '/users/', name: 'Users', component:  () => import('@/views/users/Users.vue'), meta: { title: 'Users' } },
    { path: '/profile/', name: 'Profile', component:  () => import('@/views/users/Profile.vue'), meta: { title: 'Profile' } },
  ]
})