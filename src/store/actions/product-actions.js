import service from '@/services/ProductService'

import * as types from '../mutation-types'

export const productActions = {
  getProducts({commit}) {
    commit(types.START_LOADING)
    service.getProducts().then(response => {
      if (response.status === 200) {
        console.log(response)
        commit(types.ALL_PRODUCTS_SUCCESS, response.data)
      } else {
        console.log(response.data.message)
        commit(types.CONNECTION_ERROR_MESSAGE)
      }
    }).catch(error => {
      console.log(error.message)
      commit(types.CONNECTION_ERROR_MESSAGE)
    })
  },
  // get single product
  getSingleProduct({commit}, id) {
    commit(types.START_LOADING)
    service.getSingleProduct(id).then(response => {
      if (response.status === 200) {
        console.log(response.data)
        commit(types.SINGLE_PRODUCT_SUCCESS, response.data)
      } else {
        console.log(response.data.message)
        commit(types.CONNECTION_ERROR_MESSAGE)
      }
    }).catch(error => {
      console.log(error.message)
      commit(types.CONNECTION_ERROR_MESSAGE)
    })
  },
  // Add new product
  addProduct ({commit, dispatch}, data) {
    commit(types.START_LOADING)
    service.addProduct(data).then(response => {
      if (response.status === 201) {
        console.log(response.data.message)
        dispatch('getProducts')
        commit(types.SUCCESS_MESSAGE, 'Product added!')
      } else {
        console.log(response.data.message)
        commit(types.CONNECTION_ERROR_MESSAGE)
      }
    }).catch(error => {
      console.log(error.message)
      commit(types.CONNECTION_ERROR_MESSAGE)
    })
  },
  // Update product
  updateProduct ({commit, dispatch}, payload) {
    commit(types.START_LOADING)
    service.updateProduct(payload.id, payload.data).then(response => {
      if (response.status === 200) {
        console.log(response.data)
        dispatch('getProducts')
        commit(types.SUCCESS_MESSAGE, 'Product updated')
      } else {
        console.log(response.data.message)
        commit(types.CONNECTION_ERROR_MESSAGE)
      }
    }).catch(error => {
      console.log(error.message)
      commit(types.CONNECTION_ERROR_MESSAGE)
    })
  },
  deleteProduct ({commit, dispatch}, id) {
    commit(types.START_LOADING)
    service.deleteProduct(id).then(response => {
      if (response.status === 200) {
        console.log(response.data)
        dispatch('getProducts')
        commit(types.SUCCESS_MESSAGE, 'Product deleted')
      } else {
        console.log(response.data.message)
        commit(types.CONNECTION_ERROR_MESSAGE)
      }
    }).catch(error => {
      console.log(error.message)
      commit(types.CONNECTION_ERROR_MESSAGE)
    })
  },
}
