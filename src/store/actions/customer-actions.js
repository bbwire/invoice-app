import service from '@/services/CustomerService'

import * as types from '../mutation-types'

export const customerActions = {
  getCustomers({commit}) {
    commit(types.START_LOADING)
    service.getCustomers().then(response => {
      if (response.status === 200) {
        console.log(response.data)
        commit(types.ALL_CUSTOMERS_SUCCESS, response.data)
      } else {
        console.log(response.data.message)
        commit(types.CONNECTION_ERROR_MESSAGE)
      }
    }).catch(error => {
      console.log(error.message)
      commit(types.CONNECTION_ERROR_MESSAGE)
    })
  },
  // get single customer
  getSingleCustomer({commit}, id) {
    commit(types.START_LOADING)
    service.getSingleCustomer(id).then(response => {
      if (response.status === 200) {
        console.log(response.data)
        commit(types.SINGLE_CUSTOMER_SUCCESS, response.data)
      } else {
        console.log(response.data.message)
        commit(types.CONNECTION_ERROR_MESSAGE)
      }
    }).catch(error => {
      console.log(error.message)
      commit(types.CONNECTION_ERROR_MESSAGE)
    })
  },
  // Add new customer
  addCustomer ({commit, dispatch}, data) {
    commit(types.START_LOADING)
    service.addCustomer(data).then(response => {
      if (response.status === 201) {
        console.log(response.data)
        dispatch('getCustomers')
        commit(types.SUCCESS_MESSAGE, 'Customer added')
      } else {
        console.log(response)
        commit(types.CONNECTION_ERROR_MESSAGE)
      }
    }).catch(error => {
      console.log(error.message)
      commit(types.CONNECTION_ERROR_MESSAGE)
    })
  },
  // Update customer
  updateCustomer ({commit, dispatch}, payload) {
    commit(types.START_LOADING)
    service.updateCustomer(payload.id, payload.data).then(response => {
      if (response.status === 200) {
        console.log(response.data.message)
        dispatch('getCustomers')
        commit(types.SUCCESS_MESSAGE, 'Customer info updated')
      } else {
        console.log(response.data.message)
        commit(types.CONNECTION_ERROR_MESSAGE)
      }
    }).catch(error => {
      console.log(error.message)
      commit(types.CONNECTION_ERROR_MESSAGE)
    })
  },
  deleteCustomer ({commit, dispatch}, id) {
    commit(types.START_LOADING)
    service.deleteCustomer(id).then(response => {
      if (response.status === 200) {
        console.log(response.data.message)
        dispatch('getCustomers')
        commit(types.SUCCESS_MESSAGE, 'Customer deleted!')
      } else {
        console.log(response.data.message)
        commit(types.CONNECTION_ERROR_MESSAGE)
      }
    }).catch(error => {
      console.log(error.message)
      commit(types.CONNECTION_ERROR_MESSAGE)
    })
  },
}
