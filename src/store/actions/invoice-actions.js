import service from '@/services/InvoiceService'

import * as types from '../mutation-types'

export const invoiceActions = {
  getInvoices({commit}, id) {
    commit(types.START_LOADING)
    service.getInvoices(id).then(response => {
      if (response.status === 200) {
        console.log(response.data)
        commit(types.ALL_INVOICES_SUCCESS, response.data)
      } else {
        console.log(response.data)
        commit(types.CONNECTION_ERROR_MESSAGE)
      }
    }).catch(error => {
      console.log(error.message)
      commit(types.CONNECTION_ERROR_MESSAGE)
    })
  },
  // get single invoice
  getSingleInvoice({commit}, payload) {
    commit(types.START_LOADING)
    service.getSingleInvoice(payload.customer_id, payload.id).then(response => {
      if (response.status === 200) {
        console.log(response.data)
        commit(types.SINGLE_INVOICE_SUCCESS, response.data)
      } else {
        console.log(response.data)
        commit(types.CONNECTION_ERROR_MESSAGE)
      }
    }).catch(error => {
      console.log(error.message)
      commit(types.CONNECTION_ERROR_MESSAGE)
    })
  },
  // Add new invoice
  addInvoice ({commit, dispatch}, payload) {
    commit(types.START_LOADING)
    service.addInvoice(payload.customer_id, payload.data).then(response => {
      if (response.status === 201) {
        console.log(response.data)
        dispatch('getInvoices', payload.customer_id)
        commit(types.SUCCESS_MESSAGE, 'Invoice added')
      } else {
        console.log(response.data.message)
        commit(types.CONNECTION_ERROR_MESSAGE)
      }
    }).catch(error => {
      console.log(error.message)
      commit(types.CONNECTION_ERROR_MESSAGE)
    })
  },
  // Update invoice
  updateInvoice ({commit, dispatch}, payload) {
    commit(types.START_LOADING)
    service.updateInvoice(payload.customer_id, payload.id, payload.data).then(response => {
      if (response.status === 200) {
        console.log(response.data)
        dispatch('getInvoices', payload.customer_id)
        commit(types.SUCCESS_MESSAGE, 'Invoice updated')
      } else {
        console.log(response.data)
        commit(types.CONNECTION_ERROR_MESSAGE)
      }
    }).catch(error => {
      console.log(error.message)
      commit(types.CONNECTION_ERROR_MESSAGE)
    })
  },
  deleteInvoice ({commit, dispatch}, payload) {
    commit(types.START_LOADING)
    service.deleteInvoice(payload.customer_id, payload.id).then(response => {
      if (response.status === 200) {
        console.log(response.data)
        dispatch('getInvoices', payload.customer_id)
        commit(types.SUCCESS_MESSAGE, 'Invoice deleted')
      } else {
        console.log(response.data)
        commit(types.CONNECTION_ERROR_MESSAGE)
      }
    }).catch(error => {
      console.log(error.message)
      commit(types.CONNECTION_ERROR_MESSAGE)
    })
  },
}
