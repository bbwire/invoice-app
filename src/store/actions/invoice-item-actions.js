import service from '@/services/InvoiceItemService'

import * as types from '../mutation-types'

export const invoiceItemsActions = {
  getInvoiceItems({commit}, payload) {
    commit(types.START_LOADING)
    service.getInvoiceItems(payload.customer_id, payload.invoice_id).then(response => {
      if (response.status === 200) {
        console.log(response.data)
        commit(types.ALL_INVOICE_ITEMS_SUCCESS, response.data)
        response.reduce((item_price, currentItem) => (currentItem.total + item_price), 0)
      } else {
        console.log(response.data)
        commit(types.CONNECTION_ERROR_MESSAGE)
      }
    }).catch(error => {
      console.log(error.message)
      commit(types.CONNECTION_ERROR_MESSAGE)
    })
  },
  // get single invoice
  getSingleInvoiceItem({commit}, payload) {
    commit(types.START_LOADING)
    service.getSingleInvoiceItem(payload.customer_id, payload.invoice_id, payload.id).then(response => {
      if (response.status === 200) {
        console.log(response.data)
        commit(types.SINGLE_INVOICE_SUCCESS, response.data)
      } else {
        console.log(response.data.message)
        commit(types.CONNECTION_ERROR_MESSAGE)
      }
    }).catch(error => {
      console.log(error.message)
      commit(types.CONNECTION_ERROR_MESSAGE)
    })
  },
  // Add new invoice
  addInvoiceItem ({commit, dispatch}, payload) {
    commit(types.START_LOADING)
    service.addInvoiceItem(payload.customer_id, payload.data.invoiceId, payload.data).then(response => {
      if (response.status === 201) {
        console.log(response.data)
        dispatch('getInvoiceItems', {customer_id: payload.customer_id, invoice_id: payload.data.invoiceId})
        commit(types.SUCCESS_MESSAGE, 'Item added')
      } else {
        console.log(response.data)
        commit(types.CONNECTION_ERROR_MESSAGE)
      }
    }).catch(error => {
      console.log(error.message)
      commit(types.CONNECTION_ERROR_MESSAGE)
    })
  },
  // Update invoice
  updateInvoiceItem ({commit}, payload) {
    commit(types.START_LOADING)
    service.updateInvoiceItem(payload.customer_id, payload.invoice_id, payload.id, payload.data).then(response => {
      if (response.status === 200) {
        console.log(response.data)
        commit(types.SUCCESS_MESSAGE, 'Item updated')
      } else {
        console.log(response.data)
        commit(types.CONNECTION_ERROR_MESSAGE)
      }
    }).catch(error => {
      console.log(error.message)
      commit(types.CONNECTION_ERROR_MESSAGE)
    })
  },
  deleteInvoiceItem ({commit}, payload) {
    commit(types.START_LOADING)
    service.deleteInvoiceItem(payload.customer_id, payload.invoice_id, payload.id).then(response => {
      if (response.status === 200) {
        console.log(response.data.message)
        commit(types.SUCCESS_MESSAGE, 'Invoice deleted')
      } else {
        console.log(response.data.message)
        commit(types.CONNECTION_ERROR_MESSAGE)
      }
    }).catch(error => {
      console.log(error.message)
      commit(types.CONNECTION_ERROR_MESSAGE)
    })
  },
}
