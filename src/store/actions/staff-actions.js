import service from '@/services/UserService'

import * as types from '../mutation-types'

export const staffActions = {
  getUsers ({commit}) {
    commit(types.START_LOADING)
    service.getUsers().then(response => {
      if (response.status === 200) {
        console.log(response.data)
        commit(types.ALL_USERS_SUCCESS, response.data)
      } else {
        console.log(response.data)
        commit(types.CONNECTION_ERROR_MESSAGE)
      }
    }).catch(error => {
      console.log(error.message)
      commit(types.CONNECTION_ERROR_MESSAGE)
    })
  },
  // get single user
  getSingleUser ({commit}, id) {
    commit(types.START_LOADING)
    service.getSingleUser(id).then(response => {
      if (response.status === 200) {
        console.log(response.data)
        commit(types.SINGLE_USER_SUCCESS, response.data)
      } else {
        console.log(response.data)
        commit(types.CONNECTION_ERROR_MESSAGE)
      }
    }).catch(error => {
      console.log(error.message)
      commit(types.CONNECTION_ERROR_MESSAGE)
    })
  },
  // get single user
  getCurrentUser ({commit}, id) {
    commit(types.START_LOADING)
    service.getSingleUser(id).then(response => {
      if (response.status === 200) {
        console.log(response.data)
        commit(types.CURRENT_USER_SUCCESS, response.data)
      } else {
        console.log(response.data)
        commit(types.CONNECTION_ERROR_MESSAGE)
      }
    }).catch(error => {
      console.log(error.message)
      commit(types.CONNECTION_ERROR_MESSAGE)
    })
  },
  // add user
  addUser ({commit}, data) {
    commit(types.START_LOADING)
    service.addUser(data).then(response => {
      if (response.status === 201) {
        console.log(response.data)
        commit(types.SUCCESS_MESSAGE, 'User added')
      } else {
        console.log(response.data)
        commit(types.CONNECTION_ERROR_MESSAGE)
      }
    }).catch(error => {
      console.log(error.message)
      commit(types.CONNECTION_ERROR_MESSAGE)
    })
  },
  // Update user
  updateUser ({commit, dispatch}, payload) {
    commit(types.START_LOADING)
    service.updateUser(payload.id, payload.data).then(response => {
      if (response.status === 200) {
        console.log(response.data.message)
        dispatch('getCurrentUser', payload.data.id)
        commit(types.SUCCESS_MESSAGE, 'Info updated')
      } else {
        console.log(response.data)
        commit(types.CONNECTION_ERROR_MESSAGE)
      }
    }).catch(error => {
      console.log(error.message)
      commit(types.CONNECTION_ERROR_MESSAGE)
    })
  },
  // user login
  userLogin ({commit}, data) {
    commit(types.START_LOADING)
    let self = this
    service.userLogin(data).then(response => {
      if (response.status === 200) {
        console.log(response.data)
        self._vm.$session.start()
        self._vm.$session.set('uid', response.data.id)
        location.reload()
        commit(types.SUCCESS_MESSAGE, 'Logged in successfully')
      } else {
        console.log(response.data)
        commit(types.CONNECTION_ERROR_MESSAGE)
      }
    }).catch(error => {
      console.log(error.message)
      commit(types.CONNECTION_ERROR_MESSAGE)
    })
  },
}
