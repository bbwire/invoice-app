import Vue from 'vue'
import Vuex from 'vuex'

// States
import state from './states'

// Import Actions
import { commonActions } from './actions/common-actions'
import { staffActions } from './actions/staff-actions'
import { productActions } from './actions/product-actions'
import { customerActions } from './actions/customer-actions'
import { invoiceActions } from './actions/invoice-actions'
import { invoiceItemsActions } from './actions/invoice-item-actions'

// Import Mutations
import { commonMutations } from './mutations/common-mutations'
import { majorMutations } from './mutations/major-mutations'
import { staffMutations } from './mutations/staff-mutations'
import { settingMutations } from './mutations/setting-mutations'

Vue.use(Vuex)

export default new Vuex.Store(
  {
    state: Object.assign({}, state),
    getters: {
      // isLoading: state => {
      //   state.isLoading
      // }
    },
    // Actions
    actions: Object.assign({},
      commonActions,
      staffActions,
      productActions,
      customerActions,
      invoiceActions,
      invoiceItemsActions
    ),
    // Mutations
    mutations: Object.assign({}, commonMutations, majorMutations, staffMutations, settingMutations)
  }
)