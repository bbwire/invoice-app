import * as types from '../mutation-types'

export const majorMutations = {
  // Major mutations
  [types.ALL_PRODUCTS_SUCCESS] (state, payload) {
    state.isLoading = false
    state.products = payload
  },
  [types.SINGLE_PRODUCT_SUCCESS] (state, payload) {
    state.isLoading = false
    state.single_product = payload
  },
  [types.ALL_CUSTOMERS_SUCCESS] (state, payload) {
    state.isLoading = false
    state.customers = payload
  },
  [types.SINGLE_CUSTOMER_SUCCESS] (state, payload) {
    state.isLoading = false
    state.single_customer = payload
  },
  [types.ALL_INVOICES_SUCCESS] (state, payload) {
    state.isLoading = false
    state.invoices = payload
  },
  [types.SINGLE_INVOICE_SUCCESS] (state, payload) {
    state.isLoading = false
    state.single_invoice = payload
  },
  [types.ALL_INVOICE_ITEMS_SUCCESS] (state, payload) {
    state.isLoading = false
    state.invoice_items = payload
  },
  [types.SINGLE_INVOICE_ITEM_SUCCESS] (state, payload) {
    state.isLoading = false
    state.single_invoice_item = payload
  },
}
