import Vue from 'vue'
import router from './router';
import store from './store';
import App from './App.vue'
import vuetify from './plugins/vuetify';
import VueSession from 'vue-session'
import 'material-design-icons-iconfont/dist/material-design-icons.css'

Vue.config.productionTip = false

Vue.use(VueSession)

new Vue({
  vuetify,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
