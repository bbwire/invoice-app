import Api from '@/services/Api.js'

export default {
  getInvoiceItems (customerId, id) {
    return Api().get(`/v1/customer/${customerId}/invoice/${id}/invoice_item`)
  },
  getSingleInvoiceItem (customerId, invoiceId, id) {
    return Api().get(`/v1/customer/${customerId}/invoice/${invoiceId}/invoice_item/${id}`)
  },
  addInvoiceItem (customerId, id, data) {
    return Api().post(`/v1/customer/${customerId}/invoice/${id}/invoice_item`, data)
  },
  updateInvoiceItem (customerId, invoiceId, id, data) {
    return Api().put(`/v1/customer/${customerId}/invoice/${invoiceId}/invoice_item/${id}`, data)
  },
  deleteInvoiceItem (customerId, invoiceId, id) {
    return Api().delete(`/v1/customer/${customerId}/invoice/${invoiceId}/invoice_item/${id}`)
  }
}
