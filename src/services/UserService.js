import Api from '@/services/Api.js'

export default {
  getUsers () {
    return Api().get('/v1/user')
  },
  getSingleUser (id) {
    return Api().get('/v1/user/' + id)
  },
  addUser (data) {
    return Api().post('/v1/user', data)
  },
  updateUser (id, data) {
    return Api().post('/v1/user/' + id, data)
  },
  userLogin (data) {
    return Api().post('/v1/user/login', data)
  },
}
