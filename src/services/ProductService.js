import Api from '@/services/Api.js'

export default {
  getProducts () {
    return Api().get('/v1/product')
  },
  getSingleProduct (id) {
    return Api().get('/v1/product/' + id)
  },
  addProduct (data) {
    return Api().post('/v1/product', data)
  },
  updateProduct (id, data) {
    return Api().put('/v1/product/' + id, data)
  },
  deleteProduct (id) {
    return Api().delete('/v1/product/' + id)
  }
}
