import Api from '@/services/Api.js'

export default {
  getInvoices (id) {
    return Api().get(`/v1/customer/${id}/invoice`)
  },
  getSingleInvoice (customerId, id) {
    return Api().get(`/v1/customer/${customerId}/invoice/${id}`)
  },
  addInvoice (id, data) {
    return Api().post(`/v1/customer/${id}/invoice`, data)
  },
  updateInvoice (customerId, id, data) {
    return Api().put(`/v1/customer/${customerId}/invoice/${id}`, data)
  },
  deleteInvoice (customerId, id) {
    return Api().delete(`/v1/customer/${customerId}/invoice/${id}`)
  }
}
