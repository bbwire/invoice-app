import Api from '@/services/Api.js'

export default {
  getCustomers () {
    return Api().get('/v1/customer')
  },
  getSingleCustomer (id) {
    return Api().get('/v1/customer/' + id)
  },
  addCustomer (data) {
    return Api().post('/v1/customer', data)
  },
  updateCustomer (id, data) {
    return Api().put('/v1/customer/' + id, data)
  },
  deleteCustomer (id) {
    return Api().delete('/v1/customer/' + id)
  }
}
