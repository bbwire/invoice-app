import axios from 'axios'

export default() => {
  return axios.create({
    // baseURL: `http://127.0.0.1:8000`,
    baseURL: `https://5d98d65661c84c00147d7245.mockapi.io/sp5/api`,
    withCredentials: false,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  })
}
