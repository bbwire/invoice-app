import { shallowMount } from '@vue/test-utils';
import Products from '@/views/Products';

const wrapper = shallowMount(Products)

describe('Products', () => {
  it('renders a table to display products', () => {

    expect(wrapper.find('.v-data-table').exists()).toBeTruthly()
  })

  it('renders a button to add new product', () => {

    expect(wrapper.find('.v-btn.gradient-button').text()).toBe('New product')
  })

  it('renders a button to add new product', () => {
    wrapper.find('.v-btn.gradient-button').trigger('click')
    expect(wrapper.vm.dialog).toBe(true)
  })
  
})