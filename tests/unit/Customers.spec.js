import { shallowMount } from '@vue/test-utils';
import Customers from '@/views/Customers';

const wrapper = shallowMount(Customers)

describe('Customers', () => {
  it('renders a table to display customers', () => {

    expect(wrapper.find('.v-data-table').exists()).toBeTruthly()
  })

  it('renders a button to add new customer', () => {

    expect(wrapper.find('.v-btn.gradient-button').text()).toBe('New customer')
  })

  it('renders a button to add new customer', () => {
    wrapper.find('.v-btn.gradient-button').trigger('click')
    expect(wrapper.vm.dialog).toBe(true)
  })
  
})