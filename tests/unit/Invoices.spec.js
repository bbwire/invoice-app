import { shallowMount } from '@vue/test-utils';
import Invoices from '@/views/Invoices';

const wrapper = shallowMount(Invoices, {
  data: {
    dialog: false
  }
})

describe('Invoices', () => {

  const addButton = wrapper.findAll('.star').at(3)

  it('renders a table to display invoices', () => {
    expect(wrapper.find('.v-data-table').exists()).toBeTruthly()
  })

  it('renders a button to add new invoice', () => {
    expect(wrapper.find('.v-btn.gradient-button').text()).toBe('New invoice')
  })

  it('renders a button to add new invoice', () => {
    wrapper.find('.v-btn.gradient-button').trigger('click')
    expect(wrapper.vm.dialog).toBe(true)
  })

})